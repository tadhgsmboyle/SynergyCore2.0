package net.synergyserver.synergycore.profiles;

import net.synergyserver.synergycore.SerializableLocation;
import net.synergyserver.synergycore.WeatherType;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.guis.SettingGUI;
import net.synergyserver.synergycore.settings.SettingPreferences;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bson.types.ObjectId;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Property;
import org.mongodb.morphia.annotations.Transient;

import java.util.HashMap;
import java.util.UUID;

/**
 * Represents the profile of a player in a world group.
 */
@Entity(value = "worldgroupprofiles")
public class WorldGroupProfile implements Profile {

    @Id
    private ObjectId wgpID;

    @Property("n")
    private String worldGroupName;

    @Property("pid")
    private UUID pID;
    @Property("li")
    private long lastLogIn;
    @Property("lo")
    private long lastLogOut;
    @Property("b")
    private boolean banned;
    @Property("pt")
    private long playtime;
    @Property("at")
    private long afktime;
    @Embedded("ll")
    private SerializableLocation lastLocation;
    @Property("s")
    private ObjectId prefID;
    @Embedded("hs")
    private HashMap<String, SerializableLocation> homes;
    @Property("pet")
    private Long personalTime;
    @Property("pew")
    private WeatherType personalWeather;

    @Transient
    private DataManager dm = DataManager.getInstance();
    @Transient
    private long lastPlaytimeUpdate;
    @Transient
    private long lastAfktimeUpdate;
    @Transient
    private long afktimeStart;
    @Transient
    private long afktimeEnd;
    @Transient
    private float lastPitch;
    @Transient
    private float lastYaw;
    @Transient
    private long lastAction;
    @Transient
    private SettingPreferences settingPreferences;
    @Transient
    private SettingGUI settingGUI;

    /**
     * Required constructor for Morphia to work.
     */
    public WorldGroupProfile() {}

    /**
     * Creates a new <code>WorldGroupProfile</code> with the given variables.
     *
     * @param worldGroupName The name that this corresponds to.
     * @param pID The ID of the player of this profile.
     * @param lastLogIn The last log in time of this player.
     * @param lastLogOut The last log out time of this player
     * @param lastLocation The last location of this player.
     * @param prefID The ID of a <code>SettingPreferences</code>.
     */
    public WorldGroupProfile(String worldGroupName, UUID pID, long lastLogIn, long lastLogOut,
                             SerializableLocation lastLocation, ObjectId prefID) {
        this.wgpID = new ObjectId();

        this.worldGroupName = worldGroupName;
        this.pID = pID;
        this.lastLogIn = lastLogIn;
        this.lastLogOut = lastLogOut;
        this.lastLocation = lastLocation;
        this.prefID = prefID;

        this.banned = false;
        this.homes = new HashMap<>();
    }

    @Override
    public ObjectId getID() {
        return wgpID;
    }

    /**
     * Gets the world group name that this <code>WorldGroupProfile</code> corresponds to.
     *
     * @return The world group name that this <code>WorldGroupProfile</code> corresponds to.
     */
    public String getWorldGroupName() {
        return worldGroupName;
    }

    /**
     * Sets the world group name that this <code>WorldGroupProfile</code> corresponds to.
     *
     * @param worldGroupName The world group name that this <code>WorldGroupProfile</code> corresponds to.
     */
    public void setWorldGroupName(String worldGroupName) {
        this.worldGroupName = worldGroupName;
        dm.updateField(this, WorldGroupProfile.class, "n", worldGroupName);
    }

    /**
     * Gets the Minecraft player ID of the player who owns this <code>WorldGroupProfile</code>.
     *
     * @return The ID of the player of this profile.
     */
    public UUID getPlayerID() {
        return pID;
    }

    @Override
    public long getLastLogIn() {
        return lastLogIn;
    }

    @Override
    public void setLastLogIn(long logInTime) {
        lastLogIn = logInTime;

        // Start a new playtime session
        lastPlaytimeUpdate = logInTime;

        dm.updateField(this, WorldGroupProfile.class, "li", logInTime);
    }

    @Override
    public long getLastLogOut() {
        return lastLogOut;
    }

    @Override
    public void setLastLogOut(long logOutTime) {
        lastLogOut = logOutTime;

        // Update playtime values
        if (Bukkit.getPlayer(pID) != null) {
            PlayerUtil.getProfile(pID).updateTotalPlaytime();
        }

        dm.updateField(this, WorldGroupProfile.class, "lo", logOutTime);
    }

    /**
     * Checks if this <code>WorldGroupProfile</code> is the current and active
     * <code>WorldGroupProfile</code> of the player the owns this profile.
     *
     * @return True if this profile is currently being used.
     */
    public boolean isInUse() {
        return lastLogOut < lastLogIn;
    }

    @Override
    public boolean isBanned() {
        return banned;
    }

    @Override
    public void setIsBanned(boolean banStatus) {
        banned = banStatus;
        dm.updateField(this, WorldGroupProfile.class, "b", banStatus);
    }

    /**
     * Gets the amount of time that this player has spent in this world group, in milliseconds.
     *
     * @return The amount of time that this player has spent in this world group.
     */
    public long getPlaytime() {
        return playtime;
    }

    /**
     * Sets the amount of time that this player has spent in this world group, in milliseconds.
     *
     * @param playtime The new amount of time that this player has spent in this world group.
     */
    public void setPlaytime(long playtime) {
        this.playtime = playtime;
        dm.updateField(this, WorldGroupProfile.class, "pt", playtime);
    }

    /**
     * Updates this player's playtime in this world group.
     */
    public void updatePlaytime() {
        // Cases for when the WGP is first created
        if (lastLogIn == lastLogOut && lastPlaytimeUpdate == 0) {
            long currentTime = System.currentTimeMillis();
            setPlaytime(playtime + (currentTime - lastLogIn));
            lastPlaytimeUpdate = currentTime;
            return;
        }
        if (lastLogIn == lastLogOut && lastPlaytimeUpdate > lastLogIn) {
            long currentTime = System.currentTimeMillis();
            setPlaytime(playtime + (currentTime - lastPlaytimeUpdate));
            lastPlaytimeUpdate = currentTime;
            return;
        }

        // If their playtime was already updated for the last session
        if (lastLogIn < lastLogOut && (lastLogOut <= lastPlaytimeUpdate || lastPlaytimeUpdate == 0)) {
            return;
        }

        // If the last time their playtime was updated in the middle of the session that ended
        if (lastLogIn < lastLogOut && lastLogIn < lastPlaytimeUpdate && lastPlaytimeUpdate < lastLogOut) {
            long currentTime = System.currentTimeMillis();
            setPlaytime(playtime + (lastLogOut - lastPlaytimeUpdate));
            lastPlaytimeUpdate = currentTime;
            return;
        }

        // If the last time their playtime was updated was before the session that ended
        if (lastLogIn < lastLogOut && lastPlaytimeUpdate <= lastLogIn && 0 < lastPlaytimeUpdate) {
            long currentTime = System.currentTimeMillis();
            setPlaytime(playtime + (lastLogOut - lastLogIn));
            lastPlaytimeUpdate = currentTime;
            return;
        }

        // If the last time their playtime was updated in between sessions
        if (lastLogOut < lastLogIn && ((lastLogOut <= lastPlaytimeUpdate && lastPlaytimeUpdate <= lastLogIn) || lastPlaytimeUpdate == 0)) {
            long currentTime = System.currentTimeMillis();
            setPlaytime(playtime + (currentTime - lastLogIn));
            lastPlaytimeUpdate = currentTime;
            return;
        }

        // If the last time their playtime was updated in the middle of the last session, but a new session is ongoing
        // Should never happen due to updating playtime in setLastLogIn
        if (lastLogOut < lastLogIn && lastPlaytimeUpdate <= lastLogOut && 0 < lastPlaytimeUpdate) {
            long currentTime = System.currentTimeMillis();
            setPlaytime(playtime + (lastLogOut - lastPlaytimeUpdate) + (currentTime - lastLogIn));
            lastPlaytimeUpdate = currentTime;
            return;
        }

        // If the last time their playtime was updated in the middle an ongoing session
        if (lastLogOut < lastLogIn && lastLogIn <= lastPlaytimeUpdate) {
            long currentTime = System.currentTimeMillis();
            setPlaytime(playtime + (currentTime - lastPlaytimeUpdate));
            lastPlaytimeUpdate = currentTime;
        }
    }

    /**
     * Gets the amount of time that this player has spent in this world group being AFK, in milliseconds.
     *
     * @return The amount of time that this server has spent in this world group being AFK.
     */
    public long getAfktime() {
        return afktime;
    }

    /**
     * Sets the amount of time that this player has spent in this world group being AFK, in milliseconds.
     *
     * @param afktime The new amount of time that this player has spent in this world group being AFK.
     */
    public void setAfktime(long afktime) {
        this.afktime = afktime;
        dm.updateField(this, WorldGroupProfile.class, "at", afktime);
    }

    /**
     * Updates this player's afktime in this world group.
     */
    public void updateAfktime() {
        // If their afktime was already updated for the last session
        if (afktimeStart < afktimeEnd && (afktimeEnd <= lastAfktimeUpdate || lastAfktimeUpdate == 0)) {
            return;
        }

        // If the last time their afktime was updated in the middle of the session that ended
        if (afktimeStart < afktimeEnd && afktimeStart < lastAfktimeUpdate && lastAfktimeUpdate < afktimeEnd) {
            long currentTime = System.currentTimeMillis();
            setAfktime(afktime + (afktimeEnd - lastAfktimeUpdate));
            lastAfktimeUpdate = currentTime;
            return;
        }

        // If the last time their afktime was updated was before the session that ended
        if (afktimeStart < afktimeEnd && lastAfktimeUpdate <= afktimeStart) {
            long currentTime = System.currentTimeMillis();
            setAfktime(afktime + (afktimeEnd - afktimeStart));
            lastAfktimeUpdate = currentTime;
            return;
        }

        // If the last time their afktime was updated in between sessions
        if (afktimeEnd < afktimeStart && ((afktimeEnd <= lastAfktimeUpdate && lastAfktimeUpdate <= afktimeStart) || lastAfktimeUpdate == 0)) {
            long currentTime = System.currentTimeMillis();
            setAfktime(afktime + (currentTime - afktimeStart));
            lastAfktimeUpdate = currentTime;
            return;
        }

        // If the last time their afktime was updated in the middle of the last session, but a new session is ongoing
        // Should never happen due to updating afktime in setAfktimeEnd
        if (afktimeEnd < afktimeStart && lastAfktimeUpdate <= afktimeEnd && 0 < lastAfktimeUpdate) {
            long currentTime = System.currentTimeMillis();
            setAfktime(afktime + (afktimeEnd - lastAfktimeUpdate) + (currentTime - afktimeStart));
            lastAfktimeUpdate = currentTime;
            return;
        }

        // If the last time their afktime was updated in the middle an ongoing session
        if (afktimeEnd < afktimeStart && afktimeStart <= lastAfktimeUpdate) {
            long currentTime = System.currentTimeMillis();
            setAfktime(afktime + (currentTime - lastAfktimeUpdate));
            lastAfktimeUpdate = currentTime;
        }
    }

    /**
     * Gets the time when this player went AFK in this world group, in milliseconds since the Unix epoch.
     *
     * @return The time when this player went AFK.
     */
    public long getAfktimeStart() {
        return afktimeStart;
    }

    /**
     * Sets the time when this player went AFK in this world group, in milliseconds since the Unix epoch.
     *
     * @param afktimeStart The time when this player went AFK.
     */
    public void setAfktimeStart(long afktimeStart) {
        this.afktimeStart = afktimeStart;

        // Start a new AFK session
        lastAfktimeUpdate = afktimeStart;
    }

    /**
     * Gets the time when this player stopped being AFK in this world group, in milliseconds since the Unix epoch.
     *
     * @return The time when this player stopped being AFK.
     */
    public long getAfktimeEnd() {
        return afktimeEnd;
    }

    /**
     * Sets the time when this player stopped being AFK in this world group, in milliseconds since the Unix epoch.
     *
     * @param afktimeEnd The time when this player stopped being AFK.
     */
    public void setAfktimeEnd(long afktimeEnd) {
        this.afktimeEnd = afktimeEnd;

        // Update afktime values
        if (Bukkit.getPlayer(pID) != null) {
            PlayerUtil.getProfile(pID).updateTotalAfktime();
        }
    }

    /**
     * Gets the last recorded value of the player's pitch. Used by
     * the <code>AFKTimer</code> to determine if a player is AFK.
     *
     * @return The last pitch of the player.
     */
    public float getLastPitch() {
        return lastPitch;
    }

    /**
     * Sets the recorded value of the player's pitch. Used by
     * the <code>AFKTimer</code> to determine if a player is AFK.
     *
     * @param lastPitch The new pitch of the player.
     */
    public void setLastPitch(float lastPitch) {
        this.lastPitch = lastPitch;
    }

    /**
     * Gets the last recorded value of the player's yaw. Used by
     * the <code>AFKTimer</code> to determine if a player is AFK.
     *
     * @return The last yaw of the player.
     */
    public float getLastYaw() {
        return lastYaw;
    }

    /**
     * Sets the recorded value of the player's yaw. Used by
     * the <code>AFKTimer</code> to determine if a player is AFK.
     *
     * @param lastYaw The new yaw of the player.
     */
    public void setLastYaw(float lastYaw) {
        this.lastYaw = lastYaw;
    }

    /**
     * Convenience method to compare the new direction a player
     * is facing and the last recorded direction of the player.
     *
     * @param pitch The pitch to compare.
     * @param yaw The yaw to compare.
     * @return True if the new direction is the same as the old.
     */
    public boolean isSameDirection(float pitch, float yaw) {
        return pitch == lastPitch && yaw == lastYaw;
    }

    /**
     * Gets the time of the last recorded action of the player. Used
     * by the <code>AFKTimer</code> to determine if a player is AFK.
     *
     * @return The time of the last action preformed by the player.
     */
    public long getLastAction() {
        return lastAction;
    }

    /**
     * Sets the time of the last recorded action of the player. Used
     * by the <code>AFKTimer</code> to determine if a player is AFK.
     *
     * @param lastAction The time of the new action preformed by the player.
     */
    public void setLastAction(long lastAction) {
        this.lastAction = lastAction;
    }

    /**
     * Gets the time that is overriding the player's time in this world group.
     *
     * @return The player's personal time, or null if the player's time isn't overridden.
     */
    public Long getPersonalTime() {
        return personalTime;
    }

    /**
     * Sets the time that will override the player's time in this world group.
     *
     * @param personalTime The player's new personal time.
     */
    public void setPersonalTime(Long personalTime) {
        this.personalTime = personalTime;
        dm.updateField(this, WorldGroupProfile.class, "pet", personalTime);
    }

    /**
     * Gets the weather that is overridding the player's weather in this world group.
     *
     * @return The player's personal weather, or null if the player's weather isn't overridden
     */
    public WeatherType getPersonalWeather() {
        return personalWeather;
    }

    /**
     * Sets the time that will override the player's weather in this world group.
     *
     * @param personalWeather The player's new personal weather.
     */
    public void setPersonalWeather(WeatherType personalWeather) {
        this.personalWeather = personalWeather;
        dm.updateField(this, WorldGroupProfile.class, "pew", personalWeather);
    }

    /**
     * Gets this player's afk-adjusted playtime for this world group.
     *
     * @return The difference between the player's raw playtime and their afktime.
     */
    public long getCalculatedPlaytime() {
        return getPlaytime() - getAfktime();
    }

    /**
     * Gets the last time this <code>WorldGroupProfile</code>'s playtime was updated.
     *
     * @return The last playtime update.
     */
    public long getLastPlaytimeUpdate() {
        return lastPlaytimeUpdate;
    }

    /**
     * Gets the last time this <code>WorldGroupProfile</code>'s afktime was updated.
     *
     * @return The last afktime update.
     */
    public long getLastAfktimeUpdate() {
        return lastAfktimeUpdate;
    }

    /**
     * Gets the last location of this player in the world group represented by this profile.
     *
     * @return The last location of this player.
     */
    public SerializableLocation getLastLocation() {
        return lastLocation;
    }

    /**
     * Sets the last location of this player in the world group represented by this profile.
     *
     * @param lastLocation A location to set as the last location of this player.
     */
    public void setLastLocation(SerializableLocation lastLocation) {
        this.lastLocation = lastLocation;
        dm.updateField(this, WorldGroupProfile.class, "ll", lastLocation);
    }

    /**
     * Gets the ID of the <code>SettingPreferences</code> for this player in this world group.
     *
     * @return The <code>SettingPreferences</code> ID.
     */
    public ObjectId getPrefID() {
        return prefID;
    }

    /**
     * Sets the ID of the <code>SettingPreferences</code> for this player in this world group.
     *
     * @param prefID The <code>SettingPreferences</code> ID.
     */
    public void setPrefID(ObjectId prefID) {
        this.prefID = prefID;
        dm.updateField(this, WorldGroupProfile.class, "s", prefID);
    }

    /**
     * Caches this <code>WorldGroupProfile</code>'s <code>SettingPreferences</code> from the database.
     */
    public void loadSettingPreferences() {
        if (prefID == null) {
            return;
        }
        settingPreferences = dm.getDataEntity(SettingPreferences.class, prefID);
    }

    /**
     * Creates the <code>SettingGUI</code> for this <code>WorldGroupProfile</code> and caches it.
     *
     */
    public void createSettingGUI() {
        Player player = Bukkit.getPlayer(pID);
        if (player == null) {
            return;
        }
        settingGUI = new SettingGUI(player, settingPreferences);
    }

    /**
     * Gets this <code>WorldGroupProfile</code>'s cached <code>SettingPreferences</code>.
     *
     * @return This <code>WorldGroupProfile</code>'s cached <code>SettingPreferences</code>.
     */
    public SettingPreferences getSettingPreferences() {
        return settingPreferences;
    }

    /**
     * Sets this this <code>WorldGroupProfile</code>'s <code>SettingPreferences</code>.
     *
     * @param settingPreferences The new <code>SettingPreferences</code>.
     */
    public void setSettingPreferences(SettingPreferences settingPreferences) {
        this.settingPreferences = settingPreferences;
        setPrefID(settingPreferences.getID());
    }

    /**
     * Gets the homes set in this <code>WorldGroupProfile</code>.
     *
     * @return The homes set in this <code>WorldGroupProfile</code>.
     */
    public HashMap<String, SerializableLocation> getHomes() {
        if (homes == null) {
            return new HashMap<>();
        }
        return homes;
    }

    /**
     * Sets the homes for this <code>WorldGroupProfile</code>.
     *
     * @param homes The new homes of this <code>WorldGroupProfile</code>.
     */
    public void setHomes(HashMap<String, SerializableLocation> homes) {
        this.homes = homes;
        dm.updateField(this, WorldGroupProfile.class, "hs", homes);
    }

    /**
     * Gets the <code>SettingGUI</code> that is cached for this <code>WorldGroupProfile</code>.
     *
     * @return This <code>WorldGroupProfile</code>'s <code>SettingGUI</code>.
     */
    public SettingGUI getSettingGUI() {
        return settingGUI;
    }

    /**
     * Sets the <code>SettingGUI</code> to cache for this <code>WorldGroupProfile</code>.
     *
     * @param settingGUI The <code>SettingGUI</code> to cache.
     */
    public void setSettingGUI(SettingGUI settingGUI) {
        this.settingGUI = settingGUI;
    }
}
