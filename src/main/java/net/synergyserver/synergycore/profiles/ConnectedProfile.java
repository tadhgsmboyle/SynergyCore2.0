package net.synergyserver.synergycore.profiles;

import org.bson.types.ObjectId;

/**
 * Represents a player on the corresponding service of this profile.
 */
public interface ConnectedProfile extends Profile {

    /**
     * Gets the synID for this profile.
     *
     * @return The ID of the owning user of this profile.
     */
    ObjectId getSynID();

    /**
     * Gets the name for this profile.
     *
     * @return A string representing the name used in the corresponding service of this profile.
     */
    String getCurrentName();

    /**
     * Sets the name for this profile.
     *
     * @param newName The new name to store in the databse.
     */
    void setCurrentName(String newName);

}
