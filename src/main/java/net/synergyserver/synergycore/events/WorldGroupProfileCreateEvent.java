package net.synergyserver.synergycore.events;

import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * This event is called whenever a new <code>WorldGroupProfile</code> is made.
 */
public class WorldGroupProfileCreateEvent extends Event {

    private WorldGroupProfile newWGP;
    private static final HandlerList handlers = new HandlerList();

    /**
     * Creates a new <code>WorldGroupProfileCreateEvent</code> with the given parameters.
     *
     * @param newWGP The <code>WorldGroupProfile</code> that is to be created and stored in the database.
     */
    public WorldGroupProfileCreateEvent(WorldGroupProfile newWGP) {
        this.newWGP = newWGP;
    }

    /**
     * Gets the <code>WorldGroupProfile</code> that is to be stored in the database.
     *
     * @return The <code>WorldGroupProfile</code> that is to be stored in the database.
     */
    public WorldGroupProfile getNewWGP() {
        return newWGP;
    }

    /**
     * Sets the <code>WorldGroupProfile</code> that is to be stored in the database.
     *
     * @param newWGP The new <code>WorldGroupProfile</code> that is to be stored in the database.
     */
    public void setNewWGP(WorldGroupProfile newWGP) {
        this.newWGP = newWGP;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
