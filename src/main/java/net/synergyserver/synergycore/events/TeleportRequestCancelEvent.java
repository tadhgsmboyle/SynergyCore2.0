package net.synergyserver.synergycore.events;

import net.synergyserver.synergycore.Teleport;
import org.bukkit.event.HandlerList;

/**
 * This event is called when a <code>Teleport</code> is cancelled by the sender.
 */
public class TeleportRequestCancelEvent extends TeleportRequestEvent {

    private static final HandlerList handlers = new HandlerList();

    /**
     * Creates a new <code>TeleportRequestCancelEvent</code> with the given <code>Teleport</code>.
     *
     * @param teleport The <code>Teleport</code> of this event.
     */
    public TeleportRequestCancelEvent(Teleport teleport) {
        super(teleport);
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
