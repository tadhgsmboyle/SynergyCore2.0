package net.synergyserver.synergycore.events;

import net.synergyserver.synergycore.BroadcastType;
import org.bukkit.event.HandlerList;

/**
 * This event is called whenever a broadcast is made in chat.
 */
public class ChatBroadcastEvent extends ChatEvent {

    private BroadcastType broadcastType;
    private String message;
    private static final HandlerList handlers = new HandlerList();

    /**
     * Creates a new <code>ChatBroadcastEvent</code> with the given parameters.
     *
     * @param broadcastType The <code>BroadcastType</code> of this broadcast.
     * @param message The message of this broadcast.
     */
    public ChatBroadcastEvent(BroadcastType broadcastType, String message) {
        super();
        this.broadcastType = broadcastType;
        this.message = message;
    }

    /**
     * Gets the <code>BroadcastType</code> of this broadcast.
     *
     * @return The <code>BroadcastType</code> of this broadcast.
     */
    public BroadcastType getBroadcastType() {
        return broadcastType;
    }

    /**
     * Gets the message of this broadcast.
     *
     * @return The message of this broadcast.
     */
    public String getMessage() {
        return message;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
