package net.synergyserver.synergycore.events;

import net.synergyserver.synergycore.settings.Setting;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * This event is called whenever a player changes one of their settings.
 */
public class SettingChangeEvent extends Event implements Cancellable {

    private Player player;
    private Setting setting;
    private Object oldValue;
    private Object newValue;
    private boolean cancelled;
    private static final HandlerList handlers = new HandlerList();

    /**
     * Creates a new <code>SettingChangeEvent</code> with the given paramters.
     *
     * @param player The player who changed their settings.
     * @param setting The <code>Setting</code> that was changed.
     * @param oldValue The old value of the setting.
     * @param newValue The new value of the setting.
     */
    public SettingChangeEvent(Player player, Setting setting, Object oldValue, Object newValue) {
        this.player = player;
        this.newValue = newValue;
        this.setting = setting;
        this.oldValue = oldValue;
        this.cancelled = false;
    }

    /**
     * Gets the player who changed their settings.
     *
     * @return The player of this event.
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Gets the setting that was changed.
     *
     * @return The <code>Setting</code> of this event.
     */
    public Setting getSetting() {
        return setting;
    }

    /**
     * Gets the old value of the setting that was changed.
     *
     * @return The old value of the setting.
     */
    public Object getOldValue() {
        return oldValue;
    }

    /**
     * Gets the new value of the setting that was changed.
     *
     * @return The new value of the setting.
     */
    public Object getNewValue() {
        return newValue;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
