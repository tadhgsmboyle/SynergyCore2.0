package net.synergyserver.synergycore.events;

import net.synergyserver.synergycore.Teleport;
import org.bukkit.event.HandlerList;

/**
 * This event is called whenever a player teleports directly without sending a request.
 */
public class TeleportDirectEvent extends TeleportEvent {

    private static final HandlerList handlers = new HandlerList();

    /**
     * Creates a new <code>TeleportDirectEvent</code> with the given <code>Teleport</code>.
     *
     * @param teleport The <code>Teleport</code> of this event.
     */
    public TeleportDirectEvent(Teleport teleport) {
        super(teleport);
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
