package net.synergyserver.synergycore.listeners;

import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.world.ChunkLoadEvent;

/**
 * Listens to events that occur in the world.
 */
public class WorldListener implements Listener {

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onChunkLoad(ChunkLoadEvent event) {
        // Indiscriminately remove fireworks as a temporary patch for the crash bug
        for (Entity e : event.getChunk().getEntities()) {
            if (e.getType().equals(EntityType.FIREWORK)) {
                e.remove();
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onEnttiySpawn(EntitySpawnEvent event) {
        // Indiscriminately remove fireworks as a temporary patch for the crash bug
        if (event.getEntityType().equals(EntityType.FIREWORK)) {
            event.setCancelled(true);
        }
    }

}
