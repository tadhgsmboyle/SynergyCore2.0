package net.synergyserver.synergycore.utils;

import net.minecraft.server.v1_14_R1.NBTTagCompound;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_14_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Utility to assist with operations involving <code>ItemStack</code>s.
 */
public class ItemUtil {

    /**
     * Sets the display name of the given <code>ItemStack</code>.
     *
     * @param item The <code>ItemStack</code> to rename.
     * @param name The name to set.
     */
    public static void setName(ItemStack item, String name) {
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        item.setItemMeta(meta);
    }

    /**
     * Sets a custom string of metadata on an <code>ItemStack</code>'s NBT data.
     *
     * @param item The <code>ItemStack</code> to give data to.
     * @param key The key of the data.
     * @param value The value of the data.
     * @return A copy of the <code>ItemStack</code> with the given key-value pair added.
     */
    public static ItemStack setNBTString(ItemStack item, String key, String value) {
        net.minecraft.server.v1_14_R1.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);

        // If the item has NBT data already then get it, otherwise create a new NBTTagCompound
        NBTTagCompound nbtData;
        if (nmsItem.hasTag()) {
            nbtData = nmsItem.getTag();
        } else {
            nbtData = new NBTTagCompound();
        }

        // Set the key-value pair
        nbtData.setString(key, value);
        nmsItem.setTag(nbtData);

        return CraftItemStack.asBukkitCopy(nmsItem);
    }

    /**
     * Gets a previously set custom string on a <code>ItemStack</code>'s NBT data.
     *
     * @param item The <code>ItemStack</code> to retrieve data from.
     * @param key The key of the data.
     * @return The retrieved string if one was found, or an empty string.
     */
    public static String getNBTString(ItemStack item, String key) {
        net.minecraft.server.v1_14_R1.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);

        // If the item has NBT data already then get it, otherwise create a new NBTTagCompound
        NBTTagCompound nbtData;
        if (nmsItem.hasTag()) {
            nbtData = nmsItem.getTag();
        } else {
            nbtData = new NBTTagCompound();
        }

        // Get the value
        return nbtData.getString(key);
    }

    /**
     * Gets the given <code>ItemStack</code>'s type as a string,
     * based on its<code>Material</code> and damage value.
     *
     * @param item The <code>ItemStack</code> to get the type of.
     * @return The <code>ItemStack</code>'s type.
     */
    public static String itemTypeToString(ItemStack item) {
        return item.getType().name().toLowerCase() + ":" + item.getDurability();
    }

    /**
     * Checks if the given <code>Material</code> is a type of air.
     *
     * @param mat The material to check.
     * @return True if the material is air.
     */
    public static boolean isAir(Material mat) {
        return mat.equals(Material.AIR) || mat.equals(Material.CAVE_AIR) || mat.equals(Material.VOID_AIR);
    }

}
