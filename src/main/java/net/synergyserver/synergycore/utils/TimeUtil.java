package net.synergyserver.synergycore.utils;

import java.text.DecimalFormat;

/**
 * Utility to parse and alter times given as a long that represents milliseconds.
 */
public class TimeUtil {

    private static final DecimalFormat minutesFormatter = new DecimalFormat("00");

    /**
     * Converts from A specified <code>TimeUnit</code> to another specified <code>TimeUnit</code>,
     * rounded to the nearest whole unit.
     *
     * @param fromUnit The <code>TimeUnit</code> to convert from.
     * @param toUnit The <code>TimeUnit</code> to convert to.
     * @param value The number of units to convert.
     * @return The value of the input in the specified unit, rounded to the nearest whole unit.
     */
    public static long toRoundedUnit(TimeUnit fromUnit, TimeUnit toUnit, long value) {
        double divided = (double) (value * fromUnit.getMilliseconds()) / toUnit.getMilliseconds();
        return Math.round(divided);
    }

    /**
     * Converts from A specified <code>TimeUnit</code> to another specified <code>TimeUnit</code>,
     * rounded to the given number of decimal places.
     *
     * @param fromUnit The <code>TimeUnit</code> to convert from.
     * @param toUnit The <code>TimeUnit</code> to convert to.
     * @param value The number of units to convert.
     * @param places The number of decimal places to round to. (0 for the nearest whole number, 2 to hundredths, etc)
     * @return The value of the input in the specified unit, rounded to the given number of places.
     */
    public static double toRoundedUnit(TimeUnit fromUnit, TimeUnit toUnit, long value, int places) {
        double divided = (double) (value * fromUnit.getMilliseconds()) / toUnit.getMilliseconds();
        double placeRounder = Math.pow(10, places);
        return Math.round(divided * placeRounder) / placeRounder;
    }

    /**
     * Calculates the number of units of the given unit that don't divide
     * evenly into the specified unit, rounded to the nearest whole unit.
     *
     * @param fromUnit The <code>TimeUnit</code> to convert from.
     * @param toUnit The <code>TimeUnit</code> to convert to.
     * @param value The number of units to convert.
     * @return The number of units that don't divide evenly into the specified unit, rounded to the nearest whole unit.
     */
    public static long toRoundedUnitRemainder(TimeUnit fromUnit, TimeUnit toUnit, long value) {
        return toRoundedUnit(TimeUnit.MILLI, fromUnit,
                (value * fromUnit.getMilliseconds()) % toUnit.getNext().getMilliseconds());
    }

    /**
     * Calculates the number of units of the given unit that don't divide
     * evenly into the specified unit, rounded to the given number of decimal places.
     *
     * @param fromUnit The <code>TimeUnit</code> to convert from.
     * @param toUnit The <code>TimeUnit</code> to convert to.
     * @param value The number of units to convert.
     * @param places The number of decimal places to round to. (0 for the nearest whole number, 2 to hundredths, etc)
     * @return The number of units that don't divide evenly into the specified unit, rounded to the nearest whole unit.
     */
    public static double toRoundedUnitRemainder(TimeUnit fromUnit, TimeUnit toUnit, long value, int places) {
        return toRoundedUnit(TimeUnit.MILLI, toUnit,
                (value * fromUnit.getMilliseconds()) % toUnit.getNext().getMilliseconds(), places);
    }

    /**
     * Converts milliseconds to the specified <code>TimeUnit</code>, rounded down.
     *
     * @param fromUnit The <code>TimeUnit</code> to convert from.
     * @param toUnit The <code>TimeUnit</code> to convert to.
     * @param value The number of units to convert.
     * @return The value of the input in the specified unit, rounded down.
     */
    public static long toFlooredUnit(TimeUnit fromUnit, TimeUnit toUnit, long value) {
        return (value * fromUnit.getMilliseconds()) / toUnit.getMilliseconds();
    }

    /**
     * Calculates the number of units of the given unit that don't divide evenly into the specified unit, rounded down.
     *
     * @param fromUnit The <code>TimeUnit</code> to convert from.
     * @param toUnit The <code>TimeUnit</code> to convert to.
     * @param value The number of units to convert.
     * @return The number of units that don't divide evenly into the specified unit's next unit, rounded down.
     */
    public static long toFlooredUnitRemainder(TimeUnit fromUnit, TimeUnit toUnit, long value) {
        return toFlooredUnit(TimeUnit.MILLI, toUnit,
                (value * fromUnit.getMilliseconds()) % toUnit.getNext().getMilliseconds());
    }

    /**
     * Parses a time constant from the given string.
     *
     * @param toParse The string to parse a time constant from.
     * @return The time in ticks for the constant, or -1 if none match.
     */
    public static long parseGameTimeConstant(String toParse) {
        switch (toParse.toLowerCase()) {
            case "sunrise":   return 23000L;
            case "dawn":      return 23500L;
            case "day":       return 0L;
            case "morning":   return 1000L;
            case "noon":      return 6000L;
            case "afternoon": return 9000L;
            case "dusk":      return 12000L;
            case "sunset":    return 12500L;
            case "evening":   return 13500L;
            case "night":     return 14000L;
            case "midnight":  return 18000L;
            default:          return -1L;
        }
    }

    /**
     * Converts gameticks to the corresponding in-game time in gameticks.
     *
     * @param ticks The ticks to convert.
     * @return The in-game time, in gameticks.
     */
    public static long toGameTimeTicks(long ticks) {
        return ticks % 24000;
    }

    /**
     * Parses a given string into a long that represents a time in gameticks. Supports standard
     * clock formatting (e.g. 7pm or 1:30am), and H/M formatting (e.g. 13h or 7h30pm).
     *
     * @param toParse The string to parse.
     * @return The parsed time in gameticks, or -1 if the time was unable to be parsed.
     */
    public static long parseFormattedTime(String toParse) {
        String toParseLower = toParse.toLowerCase();
        boolean isPM = toParseLower.endsWith("pm");
        boolean twelveHourFormat = true;
        String[] parts;

        // Split the hours and minutes into different elements
        if (toParseLower.endsWith("am") || toParseLower.endsWith("pm")) {
            parts = toParseLower.substring(0, toParseLower.length() - 2).split("[h:]");
        } else {
            parts = toParseLower.split("[h:]");
            twelveHourFormat = false;
        }

        // Check that there's a right number of parts
        if (parts.length < 1 || parts.length > 2) {
            return -1;
        }

        // Try to parse the hours and minutes
        int hours = 0;
        int minutes = 0;

        try {
            hours = Integer.parseInt(parts[0]);
            if (parts.length == 2) {
                minutes = Integer.parseInt(parts[1]);
            }
        } catch (NumberFormatException e) {
            return -1;
        }
        // Check if the values are properly constrained
        if (twelveHourFormat) {
            // (1:00 to 12:59)
            if (hours < 1 || hours > 12 || minutes < 0 || minutes > 59) {
                return -1;
            }
            if (hours == 12 && !isPM) {
                hours = 0;
            }
        } else {
            // (00:00 to 24:00)
            if (hours < 0 || hours > 24 || (hours == 24 && minutes > 0) || minutes < 0 || minutes > 59) {
                return -1;
            }
        }

        // Turn the hours into ticks
        long time = hours * 1000;
        if (isPM && hours != 12) {
            time += 12000;
        }
        // Add the minutes
        time += Math.round((minutes * 1000D) / 60);
        // Offset the time so it matches Minecraft's time
        time += 18000;

        // Simplify the time and return it
        return toGameTimeTicks(time);
    }

    /**
     * Parses a given string into a long that represents a time in gameticks. Supports the use of
     * time keywords (e.g. midnight and noon), raw tick values (e.g. 6500), standard clock formatting
     * (e.g. 7pm or 1:30am), and H/M formatting (e.g. 13h or 7h30pm).
     *
     * @param toParse The string to parse.
     * @return The parsed time in gameticks, or -1 if the time was unable to be parsed.
     */
    public static long parseGameTime(String toParse) {
        long time;

        // First try to parse the string as a keyword
        time = parseGameTimeConstant(toParse);

        // If the time hasn't been parsed yet then try to parse the raw value as a long
        if (time < 0) {
            try {
                time = Long.parseLong(toParse);
            } catch (NumberFormatException e) {}
        }

        // If the time hasn't been parsed yet then attempt to parse the hh?mm formats
        if (time < 0) {
            time = parseFormattedTime(toParse);
        }

        // If the time was unable to be parsed then return null, otherwise return the simplified time
        if (time < 0) {
            return -1;
        } else {
            return toGameTimeTicks(time);
        }
    }

    /**
     * Converts a specified tick number into a standard time (1:00 PM) formatted string.
     *
     * @param ticks The current tick-time for a given world.
     * @return The time-formatted string.
     */
    public static String formatGameTime(long ticks) {
        ticks = toGameTimeTicks(ticks);

        // Get the hour and offset by 6 hours
        long hour = ticks / 1000 + (ticks < 6000 ? 6 : -6);
        hour = hour % 12;
        // Get the minutes
        long minutes = (long) Math.floor((ticks % 1000) / (1000D / 60));
        // Output the formatted time
        return (hour == 0 ? 12 : hour) + ":" + minutesFormatter.format(minutes) + (ticks >= 6000 && ticks < 18000 ? " PM" : " AM");
    }

    /**
     * Parses a duration (7m3w5d) into a real-time (20390400000 milliseconds) representation of the the string.
     *
     * @param toParse The duration to parse.
     * @return The duration represented by milliseconds.
     */
    public static long parseDuration(String toParse) {
        long duration = 0;
        // Split data based on number-key relation
        String[] data = toParse.split("(?<=[^0-9])(?=[0-9])");
        // Loop over all the given data
        for (String part : data) {
            String[] result = part.split("(?<=[0-9])(?=[^0-9])");
            // Make sure the resulting data was properly split
            if (result.length != 2) {
                return 0;
            }
            // Checking this shouldn't be necessary as its already guaranteed by the regex expression
            int amount = Integer.parseInt(result[0]);
            // Parse the data
            switch (result[1].toLowerCase()) {
                case "s": case "sec": case "second": case "seconds":
                    duration += amount * TimeUnit.SECOND.milliseconds;
                    break;
                case "m": case "mi": case "min": case "minute": case "minutes":
                    duration += amount * TimeUnit.MINUTE.milliseconds;
                    break;
                case "h": case "hour": case "hours":
                    duration += amount * TimeUnit.HOUR.milliseconds;
                    break;
                case "d": case "day": case "days":
                    duration += amount * TimeUnit.DAY.milliseconds;
                    break;
                case "w": case "week": case "weeks":
                    duration += amount * (TimeUnit.DAY.milliseconds * 7);
                    break;
                case "mo": case "month": case "months":
                    duration += amount * TimeUnit.MONTH.milliseconds;
                    break;
                case "y": case "year": case "years":
                    duration += amount * TimeUnit.YEAR.milliseconds;
                    break;
                default:
                    break;
            }
        }
        // Return the total duration
        return duration;
    }

    /**
     * Units of time handled by <code>TimeUtil</code>, ordered by shorest to longest.
     */
    public enum TimeUnit {

        MILLI("millisecond", 1L),
        GAME_TICK("tick", 50L),
        SECOND("second", 1000L),
        MINUTE("minute", 60000L),
        HOUR("hour", 3600000L),
        DAY("day", 86400000L),
        MONTH("month", 2592000000L),
        YEAR("year", 31536000000L);

        private String name;
        private long milliseconds;

        TimeUnit(String name, long milliseconds) {
            this.name = name;
            this.milliseconds = milliseconds;
        }

        /**
         * Gets the name of this <code>TimeUnit</code> to use in messages.
         *
         * @return The non-pluralized name of this <code>TimeUnit</code>.
         */
        public String getName() {
            return name;
        }

        /**
         * Gets the milliseconds of this <code>TimeUnit</code>.
         *
         * @return The milliseconds of this <code>TimeUnit</code>.
         */
        public long getMilliseconds() {
            return milliseconds;
        }

        /**
         * Gets the next <code>TimeUnit</code> in duration.
         *
         * @return The next <code>TimeUnit</code>, or the same as the original if it was the longest already.
         */
        public TimeUnit getNext() {
            int index = this.ordinal();
            // If it is the largest then return the original unit
            if (index == values().length - 1) {
                return this;
            }
            // Otherwise return the next unit
            return values()[index + 1];
        }

        /**
         * Gets the previous <code>TimeUnit</code> in duration.
         *
         * @return The previous <code>TimeUnit</code>, or the same as the original if it was the shortest already.
         */
        public TimeUnit getPrev() {
            int index = this.ordinal();
            // If it is the shortest then return the original unit
            if (index == 0) {
                return this;
            }
            // Otherwise return the previous unit
            return values()[index - 1];
        }

        public TimeUnit getOffset(int offset) {
            int index = this.ordinal();

            // If the requested offset is out of range then return the closest value
            if (index + offset < 0) {
                return TimeUnit.GAME_TICK;
            }
            if (index + offset >= values().length) {
                return TimeUnit.YEAR;
            }

            // Otherwise return the offset time unit
            return values()[index + offset];
        }
    }
}
