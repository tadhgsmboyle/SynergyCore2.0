package net.synergyserver.synergycore.utils;

import net.synergyserver.synergycore.WeatherType;
import org.bukkit.Location;
import org.bukkit.World;

/**
 * Utility to assist with preforming tasks that take place in worlds.
 */
public class WorldUtil {

    /**
     * Checks if the position in a specific world would snow should downfall be on.
     *
     * @param loc The location to check in the world.
     * @return True if the specified location is in a snowy area.
     */
    public static boolean wouldSnow(Location loc) {
        return loc.getWorld().getBlockAt(loc).getTemperature() < 0.15;
    }

    /**
     * Checks if the position in a specific world would rain should downfall be on.
     *
     * @param loc The location to check in the world.
     * @return True if the specified location is in a rainy area.
     */
    public static boolean wouldRain(Location loc) {
        double temp = loc.getWorld().getBlockAt(loc).getTemperature();
        return temp >= 0.15 && temp <= 0.95;
    }

    /**
     * Gets the weather at the given location.
     *
     * @param loc The location to check in the world.
     * @return The <code>WeatherType</code> of the weather.
     */
    public static WeatherType getWeather(Location loc) {
        World world = loc.getWorld();
        // If the world isn't even raining then don't check location conditions
        if (!world.hasStorm()) {
            return WeatherType.CLEAR;
        }
        // Since the world is raining then determine what specific weather it is
        return WeatherType.getWeatherType(wouldRain(loc), world.isThundering(), wouldSnow(loc));
    }
}
