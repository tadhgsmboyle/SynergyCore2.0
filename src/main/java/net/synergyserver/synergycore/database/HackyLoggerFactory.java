package net.synergyserver.synergycore.database;

import org.mongodb.morphia.logging.Logger;
import org.mongodb.morphia.logging.LoggerFactory;

public class HackyLoggerFactory implements LoggerFactory {
    public Logger get(Class<?> aClass) {
        return new HackyLogger();
    }
}
