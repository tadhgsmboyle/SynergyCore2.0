package net.synergyserver.synergycore.database;

import net.synergyserver.synergycore.SynergyCore;
import org.mongodb.morphia.mapping.DefaultCreator;

/**
 * Used in place of Morphia's <code>DefaultCreator</code> to reference the correct ClassLoader for this plugin.
 */
public class HackyCreator extends DefaultCreator {

    /**
     * Returns the correct ClassLoader when called by Morphia.
     *
     * @return The ClassLoader of this plugin.
     */
    @Override
    protected ClassLoader getClassLoaderForClass() {
        return SynergyCore.getPlugin().getMongoHack();
    }
}
