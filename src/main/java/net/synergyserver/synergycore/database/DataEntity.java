package net.synergyserver.synergycore.database;

/**
 * Represents an object that is stored in the MongoDB database as an entity.
 */
public interface DataEntity {

    /**
     * Gets the ID for this <code>DataEntity</code>, used in the database.
     *
     * @return The ID of this <code>DataEntity</code>.
     */
    Object getID();

}
