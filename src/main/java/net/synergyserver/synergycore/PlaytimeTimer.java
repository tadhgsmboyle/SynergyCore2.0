package net.synergyserver.synergycore;

import net.synergyserver.synergycore.configs.PluginConfig;
import net.synergyserver.synergycore.events.AFKStatusChangeEvent;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergycore.utils.TimeUtil;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

public class PlaytimeTimer {

    public static Runnable playtimeUpdater = () -> {
        // Update the playtime and afktime of all players
        for (Player player : Bukkit.getOnlinePlayers()) {
            MinecraftProfile mcp = PlayerUtil.getProfile(player);
            mcp.updateTotalPlaytime();
            mcp.updateTotalAfktime();
        }
    };

    public static Runnable afkTimer = () -> {
        long timeUntilAFK = TimeUtil.toRoundedUnit(TimeUtil.TimeUnit.MINUTE, TimeUtil.TimeUnit.MILLI,
                PluginConfig.getConfig(SynergyCore.getPlugin()).getInt("afk.time_until_minutes"));

        // Loop through all players online and check their status
        for (Player player : Bukkit.getOnlinePlayers()) {
            MinecraftProfile mcp = PlayerUtil.getProfile(player);

            // Ignore them if they're afk
            if (mcp.isAFK()) {
                continue;
            }

            WorldGroupProfile wgp = mcp.getCurrentWorldGroupProfile();
            long currentTime = System.currentTimeMillis();

            // If their direction hasn't changed or they're a spectator and they haven't made an action, set them as afk
            float pitch = player.getEyeLocation().getPitch();
            float yaw = player.getEyeLocation().getYaw();
            if ((wgp.isSameDirection(pitch, yaw) || player.getGameMode().equals(GameMode.SPECTATOR)) &&
                    currentTime - wgp.getLastAction() > timeUntilAFK && currentTime - mcp.getLastLogIn() > timeUntilAFK) {
                // Set them as afk and emit an event
                wgp.setAfktimeStart(currentTime);
                AFKStatusChangeEvent event = new AFKStatusChangeEvent(false, mcp, true);
                Bukkit.getPluginManager().callEvent(event);
            }

            // Store the new direction
            wgp.setLastPitch(pitch);
            wgp.setLastYaw(yaw);
        }
    };
}
