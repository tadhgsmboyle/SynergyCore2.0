package net.synergyserver.synergycore.settings;

import net.synergyserver.synergycore.guis.Itemizable;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import org.bukkit.permissions.Permissible;

/**
 * Represents a setting that alters something.
 */
public abstract class Setting implements Itemizable {

    private String id;
    private SettingCategory category;
    private String description;
    private String permission;
    private Object defaultValue;
    private Object defaultValueNoPermission;

    /**
     * Creates a new <code>Setting</code> with the given parameters.
     *
     * @param id The identifier of this setting.
     * @param category The category of this setting.
     * @param description The description of this setting.
     * @param permission The permission to use this setting.
     * @param defaultValue The default value of this setting.
     * @param defaultValueNoPermission The default value of this setting, if the user
     *                                 does not have permission to access this setting.
     */
    protected Setting(String id, SettingCategory category, String description, String permission, Object defaultValue,
                      Object defaultValueNoPermission) {
        this.id = id;
        this.category = category;
        this.description = description;
        this.permission = permission;
        this.defaultValue = defaultValue;
        this.defaultValueNoPermission = defaultValueNoPermission;
    }

    /**
     * Gets the identifier of this <code>Setting</code> that contains only lowercase letters and underscores.
     *
     * @return A human-readable identifier of this <code>Setting</code>.
     */
    public String getIdentifier() {
        return id;
    }

    public String getDisplayName() {
        return id;
    }

    /**
     * Gets the <code>SettingCategory</code> of settings that this <code>Setting</code>
     * belongs to, used to group <code>Setting</code>s together by what they alter.
     *
     * @return The <code>SettingCategory</code> of this <code>Setting</code>.
     */
    public SettingCategory getCategory() {
        return category;
    }

    public String getDescription() {
        return description;
    }

    /**
     * Gets the required permission node to use this <code>Setting</code>.
     *
     * @return The permission node of this <code>Setting</code>.
     */
    public String getPermission() {
        return permission;
    }

    /**
     * Gets the default value of this <code>Setting</code>,
     * which is used if the user hasn't changed the setting yet.
     *
     * @return The default value of this <code>Setting</code>.
     */
    public Object getDefaultValue() {
        return defaultValue;
    }

    /**
     * Gets the default value of this <code>Setting</code> that is
     * used if the user does not have access to change this setting.
     *
     * @return The default value of this <code>Setting</code> to use if the user does not have the right permission.
     */
    public Object getDefaultValueNoPermission() {
        return defaultValueNoPermission;
    }

    /**
     * Convenience method for checking the value of this <code>Setting</code>
     * in the given <code>MinecraftProfile</code>'s current settings.
     *
     * @param minecraftProfile The <code>MinecraftProfile</code> to check.
     * @return The value of this <code>Setting</code>.
     */
    public Object getValue(MinecraftProfile minecraftProfile) {
        return getValue(minecraftProfile.getCurrentWorldGroupProfile().getSettingPreferences().getCurrentSettings(),
                minecraftProfile.getPlayer());
    }

    /**
     * Convenience method to get the value of this <code>Setting</code> in the given <code>SettingDiffs</code>.
     *
     * @param diffs The <code>SettingDiffs</code> to get the currently set value of this <code>Setting</code> from.
     * @param permissible The Permissible to test the permission of this <code>Setting</code> against.
     * @return The value of this <code>Setting</code>.
     */
    public Object getValue(SettingDiffs diffs, Permissible permissible) {
        return diffs.getValueOrDefault(this, permissible.hasPermission(permission));
    }
}
