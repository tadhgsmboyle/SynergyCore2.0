package net.synergyserver.synergycore.settings;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 * Represents a predefined option for a <code>FixedOptionsSetting</code>.
 */
public class SettingOption {

    private String id;
    private String permission;
    private String description;
    private ItemStack item;

    /**
     * Creates a new <code>SettingOption</code> with the given parameters.
     *
     * @param id The identifier of this option.
     * @param permission The permission node to see and use this option.
     * @param description The description of this option.
     * @param itemType The item to use in the setting GUI for this option.
     */
    public SettingOption(String id, String permission, String description, Material itemType) {
        this.id = id;
        this.permission = permission;
        this.description = description;
        this.item = new ItemStack(itemType, 1);
    }

    /**
     * Gets the ID of this <code>SettingOption</code>.
     *
     * @return The ID of this <code>SettingOption</code>.
     */
    public String getIdentifier() {
        return id;
    }

    /**
     * Gets the permission to view and use this <code>SettingOption</code>.
     *
     * @return The permission of this <code>SettingOption</code>.
     */
    public String getPermission() {
        return permission;
    }

    /**
     * Gets the description of this <code>SettingOption</code>.
     *
     * @return The description of this <code>SettingOption</code>.
     */
    public String getDescription() {
        return description;
    }

    public ItemStack getItem() {
        return item.clone();
    }
}
