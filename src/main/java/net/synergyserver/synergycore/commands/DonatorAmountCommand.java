package net.synergyserver.synergycore.commands;

import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "amount",
        aliases = {"count"},
        permission = "syn.donator.amount",
        usage = "/donator amount <get|set|add>",
        description = "Main command for managing donator amounts.",
        parentCommandName = "donator"
)
public class DonatorAmountCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        return false;
    }
}
