package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.SynergyPlugin;
import net.synergyserver.synergycore.configs.Message;
import org.apache.commons.lang.ArrayUtils;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Represents a <code>SubCommand</code>, which executes code
 * whenever it is called by its parent <code>MainCommand</code>.
 */
public abstract class SubCommand implements SynCommand {

    private SynergyPlugin plugin;
    private String commandName;
    private List<String> aliases;
    private String permission;
    private String usage;
    private String description;
    private int minArgs;
    private int maxArgs;
    private List<SenderType> validSenders;
    private boolean parseCommandFlags;
    private boolean executeIfInvalidSubCommand;
    private String parentCommandName;
    private HashMap<String, SubCommand> subCommands;

    /**
     * Creates a new <code>SubCommand</code> but does not populate any fields.
     * Call <code>populate()</code> after in order for this command to be fully functional.
     */
    public SubCommand() {
        this.subCommands = new HashMap<>();
    }

    public void populate(SynergyPlugin plugin) {
        this.plugin = plugin;

        CommandDeclaration commandDeclaration = getClass().getAnnotation(CommandDeclaration.class);
        this.plugin = plugin;
        this.commandName = commandDeclaration.commandName();
        this.aliases = Arrays.asList(commandDeclaration.aliases());
        this.permission = commandDeclaration.permission();
        this.usage = commandDeclaration.usage();
        this.description = commandDeclaration.description();
        this.minArgs = commandDeclaration.minArgs();
        this.maxArgs = commandDeclaration.maxArgs();
        this.validSenders = Arrays.asList(commandDeclaration.validSenders());
        this.parseCommandFlags = commandDeclaration.parseCommandFlags();
        this.executeIfInvalidSubCommand = commandDeclaration.executeIfInvalidSubCommand();
        this.parentCommandName = commandDeclaration.parentCommandName();
    }

    public Plugin getPlugin() {
        return plugin;
    }

    /**
     * Gets the name of the parent <code>SynCommand</code> of this <code>SubCommand</code>.
     *
     * @return The name of the parent <code>SynCommand</code>.
     */
    public String getParentCommandName() {
        return parentCommandName;
    }

    public void registerSubCommand(SubCommand subCommand) {
        subCommands.put(subCommand.getCommandName(), subCommand);

        for (String alias : subCommand.getAliases()) {
            subCommands.put(alias, subCommand);
        }
    }

    public String getCommandName() {
        return commandName;
    }

    public List<String> getAliases() {
        return aliases;
    }

    public String getPermission() {
        return permission;
    }

    public String getUsage() {
        return usage;
    }

    public String getDescription() {
        return description;
    }

    public int getMinArgs() {
        return minArgs;
    }

    public int getMaxArgs() {
        return maxArgs;
    }

    public List<SenderType> getValidSenders() {
        return validSenders;
    }

    public boolean shouldParseCommandFlags() {
        return parseCommandFlags;
    }

    public boolean isExecutedIfInvalidSubCommand() {
        return executeIfInvalidSubCommand;
    }

    public HashMap<String, SubCommand> getSubCommands() {
        return subCommands;
    }

    public boolean validate(CommandSender sender, String[] args) {
        // SenderType check
        if (!validSenders.contains(SenderType.getSenderType(sender))) {
            sender.sendMessage(Message.get("commands.error.wrong_sender_type"));
            return false;
        }
        // Permission check
        if (!sender.hasPermission(permission)) {
            sender.sendMessage(Message.get("commands.error.no_permission"));
            return false;
        }

        // Parse and strip flags if parseCommandFlags is true
        CommandFlags flags = null;
        String[] newArgs = args;

        if (parseCommandFlags) {
            flags = new CommandFlags(args);
            newArgs = CommandFlags.stripFlags(args);
        }

        // If this command is the parent of other commands then execute the code of those
        if (!subCommands.isEmpty()) {
            // Check if they entered in a valid SubCommand
            if (args.length > 0 && subCommands.containsKey(args[0])) {
                return subCommands.get(args[0]).validate(sender, (String[]) ArrayUtils.remove(args, 0));
            } else if (executeIfInvalidSubCommand) {
                // Check the number of arguments
                if (newArgs.length < minArgs || newArgs.length > maxArgs) {
                    sender.sendMessage(Message.format("commands.error.incorrect_syntax", usage));
                    return false;
                }

                // If the SubCommand was invalid but the MainComman should still execute then do it
                return execute(sender, newArgs, flags);
            }

            // Otherwise give the sender an incorrect syntax error
            sender.sendMessage(Message.format("commands.error.incorrect_syntax", usage));
            return false;
        }

        // Argument count check
        if (newArgs.length < minArgs || newArgs.length > maxArgs) {
            sender.sendMessage(Message.format("commands.error.incorrect_syntax", usage));
            return false;
        }

        // Otherwise, execute the code in this command
        return execute(sender, newArgs, flags);
    }
}