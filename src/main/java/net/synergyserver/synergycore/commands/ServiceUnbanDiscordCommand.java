package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.exceptions.ServiceOfflineException;
import net.synergyserver.synergycore.profiles.DiscordProfile;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.SynUser;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RateLimitException;

import java.util.UUID;

@CommandDeclaration(
        commandName = "discord",
        aliases = "dis",
        usage = "/serviceunban discord <player>",
        description = "Unbans the given user from the Discord server.",
        minArgs = 1,
        maxArgs = 1,
        parentCommandName = "serviceunban"
)
public class ServiceUnbanDiscordCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        UUID pID = PlayerUtil.getUUID(args[0], true, sender.hasPermission("vanish.see"));

        // Give an error if no player was found
        if (pID == null) {
            sender.sendMessage(Message.format("commands.error.player_not_found", args[0]));
            return false;
        }

        // Get the player's DiscordProfilew
        MinecraftProfile mcp = PlayerUtil.getProfile(pID);
        DataManager dm = DataManager.getInstance();
        SynUser synUser = dm.getDataEntity(SynUser.class, mcp.getSynID());
        DiscordProfile discordProfile = synUser.getDiscordProfile();

        // Give an error if their account is not connected to discord
        if (discordProfile == null) {
            sender.sendMessage(Message.format("commands.service_ban.error.service_not_connected", mcp.getCurrentName(), "Discord"));
            return false;
        }

        try {
            IGuild guild = SynergyCore.getDiscordServer();
            IUser user = guild.getUserByID(Long.parseLong(synUser.getDiscordID()));

            guild.pardonUser(user.getLongID());
            discordProfile.setIsBanned(false);
        } catch (ServiceOfflineException e) {
            sender.sendMessage(Message.format("commands.error.service_offline", "Discord"));
            return false;
        } catch (MissingPermissionsException|DiscordException|RateLimitException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
