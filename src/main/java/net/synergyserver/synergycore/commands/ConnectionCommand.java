package net.synergyserver.synergycore.commands;

import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "connection",
        aliases = {"connect", "connections", "services", "service", "link", "links"},
        permission = "syn.connection",
        usage = "/connection <add|remove|list>",
        description = "Main command for connecting external services.",
        validSenders = SenderType.PLAYER
)
public class ConnectionCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        return false;
    }
}
