package net.synergyserver.synergycore.commands;

import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "whitelist",
        aliases = "deny",
        permission = "syn.warp.whitelist",
        usage = "/warp whitelist <add|remove>",
        description = "Warp subcommand for managing whitelists.",
        minArgs = 1,
        validSenders = SenderType.PLAYER,
        parentCommandName = "warp"
)
public class WarpWhitelistCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        return false;
    }
}
