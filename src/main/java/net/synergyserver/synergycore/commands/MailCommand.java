package net.synergyserver.synergycore.commands;

import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "mail",
        permission = "syn.mail",
        usage = "/mail <read|delete|send|clear>",
        description = "Main command for managing mail.",
        validSenders = SenderType.PLAYER
)
public class MailCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        return false;
    }
}
