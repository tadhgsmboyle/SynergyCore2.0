package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.Warp;
import org.bukkit.command.CommandSender;

import java.util.UUID;

@CommandDeclaration(
        commandName = "warp",
        aliases = {"home", "warps"},
        permission = "syn.warp",
        usage = "/warp <tp|list|info|create|delete|privacy|whitelist|blacklist>",
        description = "Main command for warps.",
        minArgs = 1,
        validSenders = SenderType.PLAYER
)
public class WarpCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        return false;
    }

    /**
     * Used to sort warps.
     */
    public enum WarpOwnerType {
        SERVER(0, 1),
        OWN(1, 0),
        OTHER(2, 2);

        private int listOrder;
        private int targetPriority;

        WarpOwnerType(int listOrder, int targetPriority) {
            this.listOrder = listOrder;
            this.targetPriority = targetPriority;
        }

        /**
         * Gets the index of warps of this type when displayed in a list.
         *
         * @return The index of this <code>WarpOwnerType</code> for lists.
         */
        public int getListOrder() {
            return listOrder;
        }

        /**
         * Gets the priority of this type when being targeted in a command. Lower numbers means higher priority.
         *
         * @return The priority of this <code>WarpOwnerType</code> when targeted in a command.
         */
        public int getTargetPriority() {
            return targetPriority;
        }

        /**
         * Parses the <code>WarpOwnerType</code> for the owner of the given warp.
         *
         * @param warp The warp.
         * @param pID The UUID of the player to check.
         * @return The <code>WarpOwnerType</code>.
         */
        public static WarpOwnerType parseWarpOwnerType(Warp warp, UUID pID) {
            if (warp.getOwner().equals(SynergyCore.SERVER_ID)) {
                return SERVER;
            } else if (warp.getOwner().equals(pID)) {
                return OWN;
            } else {
                return OTHER;
            }
        }
    }
}
