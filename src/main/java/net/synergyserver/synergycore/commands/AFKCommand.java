package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.events.AFKStatusChangeEvent;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandDeclaration(
        commandName = "afk",
        aliases = {"away"},
        permission = "syn.afk",
        usage = "/afk",
        description = "Toggles your AFK status.",
        maxArgs = 0,
        validSenders = {SenderType.PLAYER}
)
public class AFKCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        MinecraftProfile mcp = PlayerUtil.getProfile((Player) sender);
        WorldGroupProfile wgp = mcp.getCurrentWorldGroupProfile();

        // Toggle the AFK state of the user
        if (mcp.isAFK()) {
            // End their afkness and emit an event
            wgp.setAfktimeEnd(System.currentTimeMillis());
            AFKStatusChangeEvent event = new AFKStatusChangeEvent(false, mcp, false);
            Bukkit.getPluginManager().callEvent(event);
        } else {
            // Start their afkness and emit an event
            wgp.setAfktimeStart(System.currentTimeMillis());
            AFKStatusChangeEvent event = new AFKStatusChangeEvent(false, mcp, true);
            Bukkit.getPluginManager().callEvent(event);
        }

        return true;
    }
}
