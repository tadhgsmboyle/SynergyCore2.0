package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.exceptions.ServiceOfflineException;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.StatusType;

import java.util.ArrayList;
import java.util.List;

@CommandDeclaration(
        commandName = "discord",
        aliases = {"dis"},
        permission = "syn.online.dubtrack",
        usage = "/online <discord|dubtrack>",
        description = "Lists the users online on a specific service.",
        maxArgs = 0,
        validSenders = {SenderType.PLAYER, SenderType.CONSOLE},
        parentCommandName = "online"
)
public class OnlineDiscordCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        try {
            IGuild server = SynergyCore.getDiscordServer();

            List<IUser> users = server.getUsers();
            List<String> usernames = new ArrayList<>();

            for (IUser user : users) {
                // Only add online users
                if (user.getPresence().getStatus().equals(StatusType.OFFLINE)) {
                    continue;
                }

                // Add the user name to the list and reset the color to gray for the punctuation
                usernames.add(user.getDisplayName(server) + ChatColor.GRAY);
            }

            String online = Message.createFormattedList(
                    usernames,
                    Message.get("info_colored_lists.item_color_1"),
                    Message.get("info_colored_lists.item_color_2"),
                    Message.get("info_colored_lists.grammar_color")
            );

            sender.sendMessage(Message.format("commands.online.list", "Discord", online));
            return true;
        } catch (ServiceOfflineException e) {
            sender.sendMessage(Message.format("commands.error.service_offline", "Discord"));
            return false;
        }
    }
}
