package net.synergyserver.synergycore.commands;

import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "profile",
        aliases = "whois",
        permission = "syn.profile",
        usage = "/profile <minecraft|discord|dubtrack>",
        description = "Main command for getting the profiles of players."
)
public class ProfileCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        return false;
    }
}
