package net.synergyserver.synergycore.commands;

import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "chat",
        permission = "syn.chat",
        usage = "/chat <lock|clear>",
        description = "Main command to manage chat."
)
public class ChatCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        return false;
    }

}
